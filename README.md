# Programação de Aplicativos Mobile I

#### Atividade complementar referente aos dia 17 e 18/03

## Criação de Aplicativo Magic 8 Ball
Nosso objetivo é criar um aplicativo que simule a `Bola Mágica 8`, um clássico brinquedo dos anos 80. O objetivo do brinquedo é ajudar você tomar decisões nos mais diversos cenários; quando a dúvida surgir, peça ajuda `Bola Mágica 8` que ela te responderá!

### Criação de projeto base.
1. Crie um novo projeto utilizando como Activity base a o tipo `Empty Activity`.
2. Após o build do projeto e a conclusão das atividades em segundo plano, abra o arquivo de layout no diretório `res\layout\activity_main.xml`.


### Criação de Design

#### Ajuste de cores do Aplicativo
1. Com o auxilio do site [MaterialPalette](http://www.materialpalette.com), vamos montar a paleta de cores do App.
2. Selecione as cores `DEEP PURPLE` e `LIGHT BLUE`.
3. Com as cores selecionadas, copie as cores selecionadas na paleta no campo inferior esquerdo, conforme as indicações da  imagem abaixo.
4. Abra o arquivo `res\values\colors.xm` e altere os valores conforme os códigos de cores selecionados.
5. Adicione a cor de background ao arquivo `res\values\colors.xm`.
```xml
<color name="colorBackground">#353535</color>
```
#### Adição de Recursos de Imagens ao Projeto
1. Acesse o link [Recursos de Imagens](https://drive.google.com/drive/folders/15ciy5XO0DL0c0OBRnYZAwZ9MHo6svjWT) e baixe as imagens contidas em uma pasta de sua preferencia.
2. Após baixar as imagens, selecione todas e as copie para a área de transferencia e as cole no diretório do projeto `res\drawable`; ou utilize o recurso **arrastar e soltar** para `res\drawable`.

<img src="images/img_03.png">

3. Confira se o diretório **drawable** possui a seguinte estrutura

<img src="images/img_04.png" height="300">  

### Criação de Layout
Terminamos a adição de recursos de imagens e design de cores; agora vamos montar o Layout do aplicativo.

1. Monte a interface gráfica adicionando um `ImageView` e dois `Buttons`, conforme o **blueprint** abaixo.

<img src="images/img_01.png" width="244" height="432">

2. No arquivo de layout, confira as **constrainsts** utilizadas na visualização de modo texto, selecionando a Guia **Text**.

<img src="images/img_02.png" height="100">

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/colorBackground"
    tools:context=".MainActivity">

    <ImageView
        android:id="@+id/imageViewBolaMagica"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:srcCompat="@drawable/inicio" />

    <Button
        android:id="@+id/button"
        style="@android:style/Widget.Material.Button.Colored"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginTop="16dp"
        android:layout_marginEnd="8dp"
        android:text="PERGUNTAR"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/imageViewBolaMagica" />

    <Button
        android:id="@+id/button2"
        style="@android:style/Widget.Material.Light.Button.Borderless.Colored"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginEnd="8dp"
        android:text="REINICIAR"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/button" />
</androidx.constraintlayout.widget.ConstraintLayout>
```
3. Após a conclusão da confecção do Layout, a tela do App deverá estar com a aparência da imagem abaixo; qualquer problema valide com o trecho do código acima, nele o estilo dos botões também já está configurado.

<img src="images/img_05.png" height="432">

### Time To Code
Agora o `Design` do App está concluído; é hora de programar!
Vamos aprimorar nossos conhecimentos em Java e criar o código necessário para implementar a interatividade com a **Bola Mágica**.

1. Abra o arquivo `java\br.com.etecalbertoferes\MainActivity.java`.
```java
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
```
2. Vamos transformar nossa `ImageView` em um objeto Java para podermos alterar as imagens das respostas da bola mágica programaticamente. Atualize seu código conforme o trecho abaixo.
```java
public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageViewBolaMagica);
    }
}
```

3. Agora vamos criar a função que irá realizar a troca de imagens em virtude ao toque no botão `PERGUNTAR`. Devemos criar uma função pública com uma assinatura recebendo a classe `View` como parametro para podermos associar o botão à função que iremos desenvolver. Atualize o código com o trecho abaixo.


```java
public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageViewBolaMagica);
    }

    /**
     * Funcao responsável por realizar a troca de imagens de forma aleatoria
     * alterando os recursos de imagens contidas na pasta drawable em virtude do clique no botão perguntar.
     * @param view
     */
    public void perguntar(View view){

        // Criacao de objeto para gerar números randomicos
        Random random = new Random();
        // Execução de método para gerar numeros aleatorios entre 1 e 8
        int imagem = random.nextInt(8) + 1;

        // Instrucao para gerar nome de recurso de imagem unido o valor aleatorio gerado e a String resposta_
        String nomeRecurso = String.format("resposta_%s", imagem);

        /*  Instrução responsável por obter o ID da imagem para efetuar a troca,
            com base no nome do recurso gerado acima, o tipo e sua localizacao.
         */
        int drawable = getResources().getIdentifier(nomeRecurso, "drawable",
                getPackageName());

        //  Troca de imagem
        imageView.setImageResource(drawable);
    }
  }
```

4. Agora vamos criar a função para reiniciar a `Bola Mágica` para uma nova pergunta. Atualize seu código com trecho abaixo.

```java
public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageViewBolaMagica);
    }

    /**
     * Funcao responsável por realizar a troca de imagens de forma aleatoria
     * alterando os recursos de imagens contidas na pasta drawable em virtude do clique no botão perguntar.
     * @param view
     */
    public void perguntar(View view){

        // Criacao de objeto para gerar números randomicos
        Random random = new Random();
        // Execução de método para gerar numeros aleatorios entre 1 e 8
        int imagem = random.nextInt(8) + 1;

        // Instrucao para gerar nome de recurso de imagem unido o valor aleatorio gerado e a String resposta_
        String nomeRecurso = String.format("resposta_%s", imagem);

        /*  Instrução responsável por obter o ID da imagem para efetuar a troca,
            com base no nome do recurso gerado acima, o tipo e sua localizacao.
         */
        int drawable = getResources().getIdentifier(nomeRecurso, "drawable",
                getPackageName());

        //  Troca de imagem
        imageView.setImageResource(drawable);
    }

    // Funcao para reiniciar a Bola Magica
    public void reiniciar(View view){
        imageView.setImageResource(R.drawable.inicio);
    }
}
```
#### Associar a Programação ao Design
Agora vamos associar as funções criadas na linguagem `Java` com a interface gráfica, para que o evento click do botão dispare um evento que faça a chamada da função `perguntar` e troque a imagem de forma aleatória.
1. Acesse novamente o arquivo de Layout `res\layout\activity_main.xml`.
2. Acesse a Guia **Design**.
<img src="images/img_02.png" height="100">

3. Selecione o botão `PERGUNTAR`.
4. Localize a propriedade `onClick`.
5. Na lista suspensa da propriedade `onClick` selecione a funcao **perguntar** conforme a imagem abaixo.

<img src="images/img_06.png">
6. Selecione o botão `REINICIAR`.
7. Repita o **passo 5** e selecione a função **reiniciar**.

### Teste o App em seu Smartphone
## HAVE FUN
